public class Main {
    public static void main(String[] args) {
        String userLogin = "SuperMegaUser123";
        String userPassword = "";
        Basket userBasket = new Basket();
        Product milk = new Product("milk", 69.99, 4.5);
        Product bread = new Product("white bread", 39.99, 4);
        Product salt = new Product("sea salt", 11.99, 4.1);
        userBasket.appendProduct(milk);
        userBasket.appendProduct(bread);
        userBasket.appendProduct(salt);
        User user = new User(userLogin, userPassword, userBasket);
    }
}