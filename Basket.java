import java.util.Arrays;

public class Basket {

    Product[] purchasedProducts;

    public Basket(){}

    public Basket(Product[] purchasedProducts){
        this.purchasedProducts = Arrays.copyOf(purchasedProducts, purchasedProducts.length);
    }

    public Product[] getPurchasedProducts() {
        return Arrays.copyOf(purchasedProducts, purchasedProducts.length);
    }

    public void setPurchasedProducts(Product[] purchasedProducts) {
        this.purchasedProducts = Arrays.copyOf(purchasedProducts, purchasedProducts.length);
    }

    public void appendProduct(Product product){
        Product[] dataNew = Arrays.copyOf(purchasedProducts, purchasedProducts.length + 1);
        dataNew[dataNew.length] = product;
        this.purchasedProducts = Arrays.copyOf(dataNew, dataNew.length + 1);
    }
}