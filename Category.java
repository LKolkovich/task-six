import java.util.Arrays;

public class Category {
    private String name;
    private Product[] products;

    public Category(String name, Product[] products) {
        this.name = name;
        this.products = Arrays.copyOf(products, products.length);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product[] getProducts() {
        return Arrays.copyOf(products, products.length);
    }

    public void setProducts(Product[] products) {
        this.products = Arrays.copyOf(products, products.length);
    }
}